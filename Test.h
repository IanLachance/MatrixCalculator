//
// Created by ianla on 2016-02-13.
//

#include <iostream>
#include <string>
#include <fstream>
#include "Matrix.h"

#ifndef MATRIX_CALCULATOR_TEST_H
#define MATRIX_CALCULATOR_TEST_H

class Test {
    public:
        Test();
        void test_main();
        bool test_is_equal();
        bool test_reading_from_file();
        bool test_add_two_matrices();
};

#endif //MATRIX_CALCULATOR_TEST_H
