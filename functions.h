//
// Created by ianla on 2016-02-13.
//

#include "Matrix.h"

#ifndef MATRIX_CALCULATOR_FUNCTIONS_H
#define MATRIX_CALCULATOR_FUNCTIONS_H

Matrix* read_matrix(std::string file);

#endif //MATRIX_CALCULATOR_FUNCTIONS_H
