//
// Created by ianla on 2016-02-02.
//
#include <iostream>
#include <string>
#include <fstream>

#ifndef MATRIX_CALCULATOR_MATRIX_H
#define MATRIX_CALCULATOR_MATRIX_H

const int MAX_VALUE = 4096;

class Matrix {

    friend std::ostream& operator<<(std::ostream& os, Matrix &m);

    public:
        Matrix();
        ~Matrix();
        Matrix(int, int, int*);
        bool operator==(const Matrix& matrix) const;
        Matrix operator+(const Matrix& matrix) const;
        int get_column();
        int get_row();
        int* get_matrix();

    private:
        int row;
        int column;
        int* matrix;
};

#endif //MATRIX_CALCULATOR_MATRIX_H
