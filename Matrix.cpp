//
// Created by ianla on 2016-02-02.
//

#include <stdexcept>
#include "Matrix.h"

Matrix::Matrix() {
    row = 3;
    column = 2;
    matrix = new int[row * column];
    matrix[0] = 2;
    matrix[1] = 3;
    matrix[2] = 4;
    matrix[3] = 5;
    matrix[4] = 10;
    matrix[5] = 11;
}

Matrix::Matrix(int r, int c, int *m) {
    row = r;
    column = c;
    matrix = m;
}

bool Matrix::operator==(const Matrix &matrix1) const {

    if (column != matrix1.column || row != matrix1.row) { return false; }

    for (int i = 0; i < matrix1.row; i++) {
        for (int j = 0; j < matrix1.column; j++) {
            if (matrix[i * column + j] != matrix1.matrix[i * column + j]) { return false; }
        }
    }
    return true;
}

Matrix Matrix::operator+(const Matrix &matrix1) const {

    if (column != matrix1.column || row != matrix1.row) {
        throw std::length_error("The size of the matrices are not the same!");
    }

    Matrix my_matrix;
    my_matrix.column = matrix1.column;
    my_matrix.row = matrix1.row;

    for (int i = 0; i < matrix1.row; i++) {
        for (int j = 0; j < matrix1.column; j++) {
            my_matrix.matrix[i * column + j] = matrix[i * column + j] + matrix1.matrix[i * column + j];
        }
    }

    return my_matrix;
}

std::ostream &operator<<(std::ostream &os, Matrix &m) {
    os << m.row << " " << m.column << std::endl;
    for (int i = 0; i < m.row; i++) {
        for (int j = 0; j < m.column; j++) {
            os << m.matrix[i * m.column + j] << " ";
        }
        os << std::endl;
    }
    return os;
}

int Matrix::get_column() {
    return column;
}

int Matrix::get_row() {
    return row;
}

int *Matrix::get_matrix() {
    return matrix;
}

Matrix::~Matrix() {

}