//
// Created by ianla on 2016-02-13.
//

#include <stdexcept>
#include "Test.h"
#include "functions.h"

Test::Test() {

}

bool Test::test_is_equal() {
    int matrix_test1[6] = {2,3,4,5,10,11};
    int matrix_test2[4] = {2,3,6,5};

    Matrix* my_matrix1 = new Matrix(3, 2, matrix_test1);
    Matrix* my_matrix2 = new Matrix(2, 2, matrix_test2);
    Matrix* default_matrix = new Matrix();

    bool test = (*my_matrix1) == (*default_matrix);
    bool test2 = (*my_matrix2) == (*default_matrix);
    return test && !test2;
}

bool Test::test_reading_from_file() {
    int matrix_test[12] = {1,2,3,4,5,6,7,8,9,10,11,12};
    Matrix* my_matrix1 = new Matrix(3, 4, matrix_test);
    Matrix* my_matrix2 = read_matrix("D:/Documents/Projects/C++/ultimate_calculator/test.txt");
    return (*my_matrix1) == (*my_matrix2);
}

bool Test::test_add_two_matrices() {
    int matrix_test[12] = {1,2,3,4,5,6,7,8,9,10,11,12};
    int matrix_test2[12] = {2,4,6,8,10,12,14,16,18,20,22,24};
    Matrix* my_matrix1 = new Matrix(3, 4, matrix_test);
    Matrix* my_matrix2 = read_matrix("D:/Documents/Projects/C++/ultimate_calculator/test.txt");
    Matrix* final_matrix = new Matrix(3, 4, matrix_test2);
    return (*final_matrix) == ((*my_matrix1) + (*my_matrix2));
}

void Test::test_main() {
    std::cout << "-----------\nTests for matrix_calculator program" << std::endl;
    std::cout << "--\nTEST: test_is_equal" << std::endl;
    test_is_equal();
    if(test_is_equal()) {
        std::cout << "GOOD: test_is_equal" << std::endl;
    }
    else {
        std::cout << "BAD: test_is_equal" << std::endl;
    }

    std::cout << "--\nTEST: test_reading_from_file" << std::endl;
    if(test_reading_from_file()) {
        std::cout << "GOOD: test_reading_from_file" << std::endl;
    }
    else {
        std::cout << "BAD: test_reading_from_file" << std::endl;
    }

    std::cout << "--\nTEST: test_add_two_matrices" << std::endl;
    if(test_add_two_matrices()) {
        std::cout << "GOOD: test_add_two_matrices" << std::endl;
    }
    else {
        std::cout << "BAD: test_add_two_matrices" << std::endl;
    }
    std::cout << "-----------" << std::endl;
}