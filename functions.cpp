//
// Created by ianla on 2016-02-13.
//

#include "Matrix.h"
#include "functions.h"

Matrix* read_matrix(std::string file) {
    std::ifstream my_file(file);
    int column = 0, row = 0;
    int* matrix = nullptr;
    Matrix* my_matrix = nullptr;

    if (my_file) {
        my_file >> row >> column;
        matrix = new int[row*column];
        for (int i = 0; i < row; i++) {
            for (int j = 0; j < column; j++) {
                my_file >> matrix[i*column+j];
            }
        }
        my_matrix = new Matrix(row, column, matrix);
    }
    else {
        std::cout << "WARNING: can't read matrix in file, default matrix is used" << std::endl;
        my_matrix = new Matrix();
    }
    return my_matrix;
}
